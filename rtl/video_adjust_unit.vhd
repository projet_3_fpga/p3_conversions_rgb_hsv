----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2021 07:22:27 PM
-- Design Name: 
-- Module Name: video_adjust_unit - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity video_adjust_unit is
  port (
    red_i   : in STD_LOGIC_VECTOR (7 downto 0);
    green_i : in STD_LOGIC_VECTOR (7 downto 0);
    blue_i  : in STD_LOGIC_VECTOR (7 downto 0);
    hsync_i : in STD_LOGIC;
    vsync_i : in STD_LOGIC;
    blank_i : in STD_LOGIC;
    
    hue_add : in STD_LOGIC_VECTOR (9 downto 0); -- signed
    sat_add : in STD_LOGIC_VECTOR (8 downto 0); -- signed
    val_add : in STD_LOGIC_VECTOR (8 downto 0); -- signed

    red_o   : out STD_LOGIC_VECTOR (7 downto 0);
    green_o : out STD_LOGIC_VECTOR (7 downto 0);
    blue_o  : out STD_LOGIC_VECTOR (7 downto 0);
    hsync_o : out STD_LOGIC;
    vsync_o : out STD_LOGIC;
    blank_o : out STD_LOGIC;
    
    clk : in STD_LOGIC;
    en : in STD_LOGIC
  );
end video_adjust_unit;

architecture rtl of video_adjust_unit is
  -- Signaux:

  -- RGB2HSV outputs
  signal hue : STD_LOGIC_VECTOR (8 downto 0);
  signal sat : STD_LOGIC_VECTOR (7 downto 0);
  signal val : STD_LOGIC_VECTOR (7 downto 0);

  -- Addition outputs
  signal hue_adjusted : STD_LOGIC_VECTOR (8 downto 0);
  signal sat_adjusted : STD_LOGIC_VECTOR (7 downto 0);
  signal val_adjusted : STD_LOGIC_VECTOR (7 downto 0);

  signal rst : STD_LOGIC;
begin

  rst <= not en;

  -- Instanciate RGB2HSV
  inst_rgb2hsv: entity work.rgb2hsv
    port map (
      red   => red_i,
      green => green_i,
      blue  => blue_i,

      hue => hue,
      sat => sat,
      val => val,

      hsv_valid => open,

      clk => clk,
      en => en
    );

  -- Instanciate adder
  inst_adder: entity work.adjustment_adder
    port map (
      hue_in => hue,
      sat_in => sat,
      val_in => val,

      hue_add => hue_add,
      sat_add => sat_add,
      val_add => val_add,

      hue_out => hue_adjusted,
      sat_out => sat_adjusted,
      val_out => val_adjusted,

      clk => clk,
      rst => rst
    );

  -- Instanciate HSV2RGB
  inst_hsv2rgb: entity work.hsv2rgb
    port map (
      hue => hue_adjusted,
      sat => sat_adjusted,
      val => val_adjusted,

      red   => red_o,
      green => green_o,
      blue  => blue_o,

      rgb_valid => open,

      clk => clk,
      en  => en
    );

  -- Instanciate delay
  inst_video_timing_delay: entity work.delay_vector_cycles
    generic map (
      DELAY_CYCLES => 11,
      WIDTH => 3
    )
    port map (
      input(0) => hsync_i,
      input(1) => vsync_i,
      input(2) => blank_i,

      delayed(0) => hsync_o,
      delayed(1) => vsync_o,
      delayed(2) => blank_o,

      clk => clk,
      rst => rst
    );
end rtl;
