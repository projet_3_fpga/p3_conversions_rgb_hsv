----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2021 03:10:30 PM
-- Design Name: 
-- Module Name: vga_through_conversions_integration_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_through_conversions_integration_tb is
end vga_through_conversions_integration_tb;

architecture stimulus of vga_through_conversions_integration_tb is
  constant CLK_PERIOD : time := 10 ns;

  -- Signaux:

  -- VGA outputs
  signal red        : STD_LOGIC_VECTOR (7 downto 0);
  signal green      : STD_LOGIC_VECTOR (7 downto 0);
  signal blue       : STD_LOGIC_VECTOR (7 downto 0);
  signal hsync      : STD_LOGIC;
  signal vsync      : STD_LOGIC;
  signal blank      : STD_LOGIC;

  -- RGB2HSV outputs
  signal hue : STD_LOGIC_VECTOR (8 downto 0);
  signal sat : STD_LOGIC_VECTOR (7 downto 0);
  signal val : STD_LOGIC_VECTOR (7 downto 0);

  -- Adjustment register constants: 0,0,0
  constant HUE_ADD : integer := 0;
  constant SAT_ADD : integer := 0;
  constant VAL_ADD : integer := 0;

  -- Addition outputs
  signal hue_adjusted : STD_LOGIC_VECTOR (8 downto 0);
  signal sat_adjusted : STD_LOGIC_VECTOR (7 downto 0);
  signal val_adjusted : STD_LOGIC_VECTOR (7 downto 0);

  -- HSV2RGB outputs
  signal red_adjusted   : STD_LOGIC_VECTOR (7 downto 0);
  signal green_adjusted : STD_LOGIC_VECTOR (7 downto 0);
  signal blue_adjusted  : STD_LOGIC_VECTOR (7 downto 0);

  -- Delay outputs
  signal hSync_delayed : STD_LOGIC;
  signal vSync_delayed : STD_LOGIC;
  signal blank_delayed : STD_LOGIC;

  -- clk, rst
  signal clk : STD_LOGIC;
  signal rst : STD_LOGIC;
  signal en  : STD_LOGIC;
begin
  -- Drive clk, rst
  rst <= '1', '0' after 2*CLK_PERIOD;
  en <= not rst;

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  -- Instanciate VGA
  inst_vga: entity work.vga
    port map (
      pixelClock => clk,
      Red        => red,
      Green      => green,
      Blue       => blue,
      hSync      => hsync,
      vSync      => vsync,
      blank      => blank
    );

  -- Instanciate RGB2HSV
  inst_rgb2hsv: entity work.rgb2hsv
    port map (
      red   => red,
      green => green,
      blue  => blue,

      hue => hue,
      sat => sat,
      val => val,

      hsv_valid => open,

      clk => clk,
      en => en
    );

  -- Instanciate adder
  inst_adder: entity work.adjustment_adder
    port map (
      hue_in => hue,
      sat_in => sat,
      val_in => val,

      hue_add => std_logic_vector(to_signed(HUE_ADD, 10)),
      sat_add => std_logic_vector(to_signed(SAT_ADD, 9)),
      val_add => std_logic_vector(to_signed(VAL_ADD, 9)),

      hue_out => hue_adjusted,
      sat_out => sat_adjusted,
      val_out => val_adjusted,

      clk => clk,
      rst => rst
    );

  -- Instanciate HSV2RGB
  inst_hsv2rgb: entity work.hsv2rgb
    port map (
      hue => hue_adjusted,
      sat => sat_adjusted,
      val => val_adjusted,

      red   => red_adjusted,
      green => green_adjusted,
      blue  => blue_adjusted,

      rgb_valid => open,

      clk => clk,
      en  => en
    );

  -- Instanciate delay
  inst_video_timing_delay: entity work.delay_vector_cycles
    generic map (
      DELAY_CYCLES => 11,
      WIDTH => 3
    )
    port map (
      input(0) => hsync,
      input(1) => vsync,
      input(2) => blank,

      delayed(0) => hsync_delayed,
      delayed(1) => vsync_delayed,
      delayed(2) => blank_delayed,

      clk => clk,
      rst => rst
    );

  -- Outputs should be approximately identical to input, delayed by 12 cycles

end stimulus;
