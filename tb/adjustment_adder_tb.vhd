----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/04/2021 05:58:45 PM
-- Design Name: 
-- Module Name: adjustment_adder_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adjustment_adder_tb is
end adjustment_adder_tb;

architecture stimulus of adjustment_adder_tb is
  constant CLK_PERIOD : time := 10 ns;

  -- Déclaration des signaux du DUT
  signal hue_in : std_logic_vector(8 downto 0);
  signal sat_in : std_logic_vector(7 downto 0);
  signal val_in : std_logic_vector(7 downto 0);

  signal hue_add : std_logic_vector(9 downto 0);
  signal sat_add : std_logic_vector(8 downto 0);
  signal val_add : std_logic_vector(8 downto 0);

  signal hue_out : std_logic_vector(8 downto 0);
  signal sat_out : std_logic_vector(7 downto 0);
  signal val_out : std_logic_vector(7 downto 0);

  signal clk : std_logic;
  signal rst : std_logic;
  
  -- Integer signals
  signal hue_in_int : integer range 0 to 383;
  signal sat_in_int : integer range 0 to 255;
  signal val_in_int : integer range 0 to 255;

  signal hue_add_int : integer range 0 to 383;
  signal sat_add_int : integer range 0 to 255;
  signal val_add_int : integer range 0 to 255;
begin

  -- Instanciation DUT
  DUT: entity work.adjustment_adder
    port map (
      hue_in  => hue_in,
      sat_in  => sat_in,
      val_in  => val_in,

      hue_add => hue_add,
      sat_add => sat_add,
      val_add => val_add,

      hue_out => hue_out,
      sat_out => sat_out,
      val_out => val_out,
   
      clk => clk,
      rst => rst
    );

  -- Drive clk
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  hue_in <= std_logic_vector(to_unsigned(hue_in_int, hue_in'length));
  sat_in <= std_logic_vector(to_unsigned(sat_in_int, sat_in'length));
  val_in <= std_logic_vector(to_unsigned(val_in_int, val_in'length));

  hue_add <= std_logic_vector(to_signed(hue_add_int, hue_add'length));
  sat_add <= std_logic_vector(to_signed(sat_add_int, sat_add'length));
  val_add <= std_logic_vector(to_signed(val_add_int, val_add'length));

  -- Test:
  testbench: process
  begin
    rst <= '1';
    wait for 2*CLK_PERIOD;
    rst <= '0';

    -- Cas: pas de problème de limite, ajustements positifs
    hue_in_int <= 100;
    sat_in_int <= 100;
    val_in_int <= 100;

    hue_add_int <= 1;
    sat_add_int <= 2;
    val_add_int <= 3;
    wait for CLK_PERIOD;

    -- Cas: pas de problème de limite, ajustements négatifs
    hue_in_int <= 100;
    sat_in_int <= 100;
    val_in_int <= 100;

    hue_add_int <= -25;
    sat_add_int <= -50;
    val_add_int <= -75;
    wait for CLK_PERIOD;

    -- Cas: limite haut
    hue_in_int <= 200;
    sat_in_int <= 200;
    val_in_int <= 200;

    hue_add_int <= 200;
    sat_add_int <= 200;
    val_add_int <= 200;
    wait for CLK_PERIOD;
    
    -- Cas: limite bas
    hue_in_int <= 100;
    sat_in_int <= 100;
    val_in_int <= 100;

    hue_add_int <= -250;
    sat_add_int <= -250;
    val_add_int <= -250;
    wait for CLK_PERIOD;
        
    wait;
  end process testbench;
end stimulus;
