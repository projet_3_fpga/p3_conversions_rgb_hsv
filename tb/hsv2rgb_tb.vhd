----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2021 09:56:55 AM
-- Design Name: 
-- Module Name: hsv2rgb_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hsv2rgb_tb is
end hsv2rgb_tb;

architecture stimulus of hsv2rgb_tb is
  constant CLK_PERIOD : time := 10 ns;

  signal hue : STD_LOGIC_VECTOR (8 downto 0);
  signal sat : STD_LOGIC_VECTOR (7 downto 0);
  signal val : STD_LOGIC_VECTOR (7 downto 0);

  signal hue_int : integer range 0 to 383;
  signal sat_int : integer range 0 to 255;
  signal val_int : integer range 0 to 255;

  signal red : STD_LOGIC_VECTOR (7 downto 0);
  signal green : STD_LOGIC_VECTOR (7 downto 0);
  signal blue : STD_LOGIC_VECTOR (7 downto 0);

  signal rgb_valid : STD_LOGIC;

  signal clk : STD_LOGIC;
  signal en : STD_LOGIC;

begin

  -- Instanciate DUT
  DUT: entity work.hsv2rgb
    port map(
      hue => hue,
      sat => sat,
      val => val,

      red => red,
      green => green,
      blue => blue,

      rgb_valid => rgb_valid,

      clk => clk,
      en => en
    );

  -- Drive clk
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  -- Test: séquence de couleurs
  hue <= std_logic_vector(to_unsigned(hue_int, hue'length));
  sat <= std_logic_vector(to_unsigned(sat_int, sat'length));
  val <= std_logic_vector(to_unsigned(val_int, val'length));

  test_bench: process is
  begin
    en <= '0';
    wait for 2*CLK_PERIOD;
    en <= '1';

    -- Couleur quelconque:
    -- R 35
    -- G 41
    -- B 254
    hue_int <= 318;
    sat_int <= 220;
    val_int <= 254;
    wait for 20*CLK_PERIOD;
    en <= '0';

    wait for CLK_PERIOD;
    en <= '1';
    wait for 10*CLK_PERIOD;

    -- Noir:
    --  R 0
    --  G 0
    --  B 0
    hue_int <= 0;
    sat_int <= 0;
    val_int <= 0;
    wait for CLK_PERIOD;

    -- Cyan:
    --  R 0
    --  G 255
    --  B 255
    hue_int <= 256;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Jaune:
    --  R 255
    --  G 255
    --  B 0
    hue_int <= 128;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Magenta:
    --  R 255
    --  G 0
    --  B 255
    hue_int <= 0;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Rouge:
    --  R 255
    --  G 0
    --  B 0
    hue_int <= 64;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Vert:
    --  R 0
    --  G 255
    --  B 0
    hue_int <= 192;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Bleu:
    --  R 0
    --  G 0
    --  B 255
    hue_int <= 320;
    sat_int <= 255;
    val_int <= 255;
    wait for CLK_PERIOD;

    -- Bleu foncé:
    --  R 0
    --  G 0
    --  B 108
    hue_int <= 320;
    sat_int <= 255;
    val_int <= 108;
    wait for CLK_PERIOD;

    wait;
  end process test_bench;

end stimulus;

